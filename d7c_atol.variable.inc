<?php

/**
 * @file
 * Variable module hooks.
 */

// Payment types codes.
// Электронный.
define('D7C_ATOL_PAYMENT_TYPE_CYBERCASH', 1);
define('D7C_ATOL_PAYMENT_TYPE_CYBERCASH_LABEL', t('Cybercash'));

// Extended payment types
// todo: It should be explained at least or able to set names.
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_2', 2);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_2_LABEL', t('Extended @num', array('@num' => 2)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_3', 3);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_3_LABEL', t('Extended @num', array('@num' => 3)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_4', 4);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_4_LABEL', t('Extended @num', array('@num' => 4)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_5', 5);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_5_LABEL', t('Extended @num', array('@num' => 5)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_6', 6);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_6_LABEL', t('Extended @num', array('@num' => 6)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_7', 7);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_7_LABEL', t('Extended @num', array('@num' => 7)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_8', 8);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_8_LABEL', t('Extended @num', array('@num' => 8)));
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_9', 9);
define('D7C_ATOL_PAYMENT_TYPE_EXTENDED_9_LABEL', t('Extended @num', array('@num' => 9)));

/**
 * Implements hook_variable_group_info().
 */
function d7c_atol_variable_group_info() {
  $groups['d7c_atol_settings'] = array(
    'title' => 'Atoll settings',
    'description' => t('Main Atoll  integration settings.'),
    'access' => D7C_ATOL_PERMISSION,
  );
  $groups['d7c_atol_correspondence'] = array(
    'title' => 'Payment types',
    'description' => t('Payment types correspondence.'),
    'access' => D7C_ATOL_PERMISSION,
  );
  $groups['d7c_atol_fields_correspondence'] = array(
    'title' => 'Customer profiles fields correspondence.',
    'description' => t('Correspondence of available customer profiles fields and data we need to send to Atol.'),
    'access' => D7C_ATOL_PERMISSION,
  );

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function d7c_atol_variable_info($options) {
  $variables['d7c_atol_login'] = array(
    'type' => 'string',
    'title' => t('Login', array(), $options),
    'default' => '',
    'description' => t('Your login in @atol_site, which you can get in your Atol personal cabinet.', array('@atoll_site' => 'https://online.atol.ru'), $options),
    'group' => 'd7c_atol_settings',
    'localize' => TRUE,
    'required' => TRUE,
  );
  $variables['d7c_atol_password'] = array(
    'type' => 'string',
    'title' => t('Password', array(), $options),
    'default' => '',
    'description' => t('Your password in @atol_site, which you can get in your Atol personal cabinet.', array('@atoll_site' => D7C_ATOL_SERVICE_URL), $options),
    'group' => 'd7c_atol_settings',
    'localize' => TRUE,
    'required' => TRUE,
  );
  $variables['d7c_atol_group_code'] = array(
    'type' => 'string',
    'title' => t('Cash registers group code', array(), $options),
    'default' => '',
    'description' => t('Atol cash registers group code.', array(), $options),
    'group' => 'd7c_atol_settings',
    'localize' => TRUE,
    'required' => TRUE,
  );
  $variables['d7c_atol_inn'] = array(
    'type' => 'string',
    'title' => t('Organisation TIN', array(), $options),
    'default' => '',
    'description' => t('Organisation Taxpayer Identification Number. Used for preventing of mistaken cheque registering.', array(), $options),
    'group' => 'd7c_atol_settings',
    'localize' => TRUE,
    'required' => TRUE,
    'element' => array(
      '#maxlength' => 12,
      '#size' => 12,
    ),
    'validate callback' => 'd7c_atol_inn_validate',
  );
  $variables['d7c_atol_payment_address'] = array(
    'type' => 'string',
    'title' => t('Payment place address', array(), $options),
    'default' => '',
    'description' => t('Used for preventing of mistaken cheque registering.', array(), $options),
    'group' => 'd7c_atol_settings',
    'element' => array(
      '#maxlength' => 256,
    ),
    'localize' => TRUE,
    'required' => TRUE,
  );
  $variables['d7c_atol_callback_url'] = array(
    'type' => 'string',
    'title' => t('Callback URL', array(), $options),
    'default' => '',
    'description' => t('If set, response will be send bu POST to this url.', array(), $options),
    'element' => array(
      '#maxlength' => 256,
    ),
    'group' => 'd7c_atol_settings',
    'localize' => '',
  );
  $variables['d7c_atol_sno'] = array(
    'type' => 'select',
    'title' => t('Taxation type', array(), $options),
    'options' => array(
      'osn' => t('Common'),
      'usn_income' => t('Simplified'),
      'usn_income_outcome' => t('Simplified (income minus expenses)'),
      'envd' => t('Single tax for imputed income'),
      'esn' => t('Single agricultural tax'),
      'patent' => t('Patent taxation'),
    ),
    'element' => array(
      '#empty_option' => t('- Choose -'),
    ),
    'default' => 'none',
    'description' => t('Optional if the organisation has only one taxation type.', array(), $options),
    'group' => 'd7c_atol_settings',
    'localize' => TRUE,
  );
  $variables['d7c_atol_tax'] = array(
    'type' => 'select',
    'title' => t('Taxation number'),
    'description' => t('Checkout machine taxation number'),
    'group' => 'd7c_atol_settings',
    'options' => array(
      'none' => t('Without VAT'),
      'vat0' => t('VAT 0%'),
      'vat10' => t('VAT 10%'),
      'vat18' => t('VAT 18%'),
      'vat110' => t('VAT 10/110'),
      'vat118' => t('VAT 18/118'),
    ),
    'element' => array(
      '#empty_option' => t('- Choose -'),
    ),
    'required' => TRUE,

    'localize' => TRUE,
  );

  foreach (commerce_payment_methods() as $id => $method) {
    $payment = commerce_payment_method_instance_load("$id|commerce_payment_$id");
    $variables['d7c_atol_payment_type_' . $payment['method_id']] = array(
      'type' => 'select',
      'title' => $payment['title'],
      'group' => 'd7c_atol_correspondence',
      'options' => array(
        D7C_ATOL_PAYMENT_TYPE_CYBERCASH => D7C_ATOL_PAYMENT_TYPE_CYBERCASH_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_2 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_2_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_3 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_3_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_4 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_4_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_5 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_5_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_6 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_6_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_7 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_7_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_8 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_8_LABEL,
        D7C_ATOL_PAYMENT_TYPE_EXTENDED_9 => D7C_ATOL_PAYMENT_TYPE_EXTENDED_9_LABEL,
      ),
      'element' => array(
        '#empty_option' => t('- Choose -'),
      ),
      'required' => TRUE,

      'localize' => TRUE,
    );
  }

  foreach (field_info_instances('commerce_customer_profile') as $customer_profile_key => $customer_profile_fields) {
    foreach ($customer_profile_fields as $customer_profile_field) {
      $fields["{$customer_profile_key}:{$customer_profile_field['field_name']}"] = $customer_profile_field['label'] . " ($customer_profile_key)";
    }
  }
  $variables['d7c_atol_customer_phone_field'] = array(
    'type' => 'select',
    'title' => t('Choose customer profile phone field'),
    'options' => $fields,
    'description' => t('Customer profile id is showed in brackets.'),
    'group' => 'd7c_atol_fields_correspondence',
    'element' => array(
      '#empty_option' => t('- Choose -'),
    ),
    'required' => TRUE,

    'localize' => TRUE,
  );

  return $variables;
}

/**
 * Validate callback for d7c_atol_inn variable.
 *
 * @see d7c_atol_variable_info
 */
function d7c_atol_inn_validate($element, &$form_state, $form) {
  if (empty($element['#parents'])) {
    $element['#parents'] = array();
  }
  if (!is_numeric($element['value'])) {
    form_error($element, t('!title must be numeric.', array('!title' => $element['title'])));
  }
  if (strlen($element['value']) !== 10 && strlen($element['value']) !== 12) {
    form_error($element, t('!title has incorrect length of @length symbols. It may consist of 10 or 12 digits.', array(
      '!title' => $element['title'],
      '@length' => strlen($element['value']),
    )));
  }
}
