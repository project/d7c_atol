-- SUMMARY --

This module provides online registering of checkout receipts,
created in your internet shop, via Atol service (https://online.atol.ru)
according to last edition of federal law №54-FZ
(https://rg.ru/2016/07/12/kassa-dok.html).

-- REQUIREMENTS --

* commerce
* commerce_payment
* variable
* entity

-- INSTALLATION --

Install as usual, see http://drupal.org/node/895232 for further information

-- CONFIGURATION --

* Config main settings on page (admin/commerce/config/atol).
* Set correspondence of your available payment types and common payment types
of Atol system (admin/commerce/config/atol/payment_correspondence).
* Set customer phone field among all available customer profiles fields
(admin/commerce/config/atol/fields_correspondence).

-- CONTACT --

Current maintainers:
* Aleksey Zubenko (Alex Zu) - https://www.drupal.org/user/1797134
